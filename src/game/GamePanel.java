// Demian Ervin - István

// A játékpanel implementálása

package game;

import dialog.GameOverDialog;
import util.*;
import util.Canvas;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

public class GamePanel extends Canvas implements Runnable {
    private final GameFrame parentGameFrame;
    private final ArrayList<Droplet> droplets = new ArrayList<>();
    private final ArrayList<Fireball> fireballs = new ArrayList<>();
    private final ArrayList<Phoenix> phoenixesFromLeft = new ArrayList<>();
    private final ArrayList<Phoenix> phoenixesFromRight = new ArrayList<>();
    private final int[] phoenixPositions = {0, 130, 260, 390, 520};
    private final Pear pear = new Pear(Common.CUSTOM_WIDTH / 2 - 50, Common.CUSTOM_HEIGHT - 150, INIT_SPEED);
    private final Sprite background = new Background(0, 0);
    private final String playerName;

    private int currentY;
    private int lives = LIVES;
    private int level = 1;
    private int score;
    private int maxPhoenixes = INIT_MAX_PHOENIXES;
    private int writePause = 0;
    private boolean pause;
    private boolean bgMusicPlays;
    private boolean isRunning;
    private Thread gamePanelThread;
    private Thread gameOverThread;
    private Controller bgMusicController;
    private Font customFont;

    // konstruktor, beállítja a panel méretét, a játékos nevét és a JFrame-et, amibe bele lett téve
    // utóbbit azért, hogy a játék lejárta után el tudja tűntetni a játékablakot
    public GamePanel(GameFrame parentGameFrame, String playerName) {
        setPreferredSize(new Dimension(Common.CUSTOM_WIDTH, Common.CUSTOM_HEIGHT));
        this.parentGameFrame = parentGameFrame;
        this.playerName = playerName;
    }

    // Runnable, kód futásának metódusa
    @Override
    public void run() {
        // inicializáljuk a megfelelő változókat
        initialise();

        // amíg a játéknak nincs vége
        while (isRunning) {
            // ha nem aktív a szüneteltetés
            if (!pause) {
                writePause = 0;     // visszaállítjuk a szünet szöveg kiírásához használt változót
                // lementjük, mennyi idő kell egy legeneráláshoz, ezt felhasználva adjuk meg, hogy mennyit várakozzunk
                // két képfrissítés között
                long startTime = System.currentTimeMillis();

                updateGame();       // frissítjuk a folyamatban levő játék változóit
                renderGame();       // kirajzoljuk a játék friss képét

                long endTime = System.currentTimeMillis() - startTime;
                long waitTime = MILLISECONDS / FPS - endTime / MILLISECONDS;

                // várakozunk, utána újabb képfrissítés
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // ha aktív a szüneteltetés
            } else {
                try {
                    writePause = (writePause + 1) % 20;     // egy másodpercig kiírjuk a szünet szöveget, egy másodpercig nem
                    renderGame();                           // mindegyik esetben kirajzoljuk az esetleges új képet
                    Thread.sleep(100);                // minden tizedmásodpercben ellenőrizzük, aktív-e még a szüneteltetés
                    if (bgMusicPlays) {                      // ha akív, megpróbáljuk leállítani a zenét, ha még nincs megállva
                        bgMusicController.stop();
                        bgMusicPlays = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // amikor elvesztettük az összes életet, lejátszódik a game over zene egy másik szál által
        // bevárjuk a game over zene szálat
        try {
            gameOverThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // megjelenítjük a game over párbeszédablakot
        new GameOverDialog();
        // amikor abból kilépünk valamilyen módon, a játékablakot is dispose-oljuk
        parentGameFrame.dispose();
    }

    // low-level AWT metódus, a játék futásának hoz létre egy szálat, amennyiben még nincs létrehozva, és elindítja azt
    @Override
    public void addNotify() {
        super.addNotify();
        if (gamePanelThread == null) {
            gamePanelThread = new Thread(this);
        }
        gamePanelThread.start();
    }

    // játék változóit inicializáló metódus
    private void initialise() {
        isRunning = true;       // a run metódussal ezen változón keresztül közöljük, hogy a játék fut, tehát a ciklus elindulhat
        // és egy darabig valószínűleg nem is kell megálljon

        // példányosítunk egy zene kontrollert a háttérzenére, amit még nem indítunk el
        bgMusicController = new Controller("assets/audio/Brodyquest.wav", true, MUSIC_DEC);

        // beállitjuk a custom betűtípust a panelen
        customFont = new MarioFont().getCustomMarioFont(18f);
        setFont(customFont);
    }

    // minden gombnyomást figyelő metódus
    @Override
    protected void onKeyPressed(KeyEvent keyEvent) {
        // csak akkor figyelünk arra, hogy milyen gomb lett a játékpanelen belül lenyomva, ha még a körtének maradt élete
        if (lives > 0) {
            if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE && droplets.size() <= MAX_DROPLETS && !pause) {
                // ha szóközt nyom a játékos, új vízcseppet jelenítünk meg, amennyiben az aktív vízcsepek száma
                // nem haladja meg a Common interface-ben megadott számot, és ha a játék nincs szüneteltetve
                // ilyenkor kiadjuk a lövés hangot egy új szálon, hogy attól függetlenül továbbra is tudjon frissülni a játék
                Controller dropletFXController = new Controller("assets/audio/pew.wav", false, FX_DEC);
                new Thread(dropletFXController).start();

                // hozzáadjuk az aktív vízcseppekhez az új vizcseppet, amit a körte függvényében megfelelő pozícióra generáltunk
                droplets.add(new Droplet(pear.getX() + 45, pear.getY(), getRandomValue(MIN_DROPLET_SPEED, MAX_DROPLET_SPEED)));
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT && !pause) {
                // ha a játék nincs szüneteltetve, és a bal nyíl gomb lett lenyomva, karakterünket balra mozdítjuk
                pear.moveLeft();
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT && !pause) {
                // ha a játék nincs szüneteltetve, és a jobb nyíl gomb lett lenyomva, karakterünket jobbra mozdítjuk
                pear.moveRight();
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_ESCAPE) {
                // ha az escape gombot nyomja meg a játékos, szüneteltetjük a játékot
                // azaz átállítjuk a szünet boolean változót, és lejátszuk a szüneteltetési hangeffektet
                pause = !pause;
                Controller pauseSound = new Controller("assets/audio/pause.wav", false, FX_DEC);
                new Thread(pauseSound).start();
            }
        }
    }

    // amikor elenged a felhasználó egy gombot (jobbra/balra), visszaállítjuk a karakterünk mozgási sebességét
    @Override
    protected void onKeyReleased(KeyEvent keyEvent) {
        pear.speedReset();
    }

    // rendszermetódus, a háttérben ciklikusan frissíti a kirajzolt képkockát a megfelelő játékváltozók alapján
    @Override
    protected void onDraw(Graphics2D graphics2D) {
        background.draw(graphics2D);    // kirajzoljuk a hátteret
        pear.draw(graphics2D);          // kirajzoljuk a karakterünket

        // kirajzoljuk az aktív bal oldali főnixeket
        for (Phoenix phoenix : phoenixesFromLeft) {
            phoenix.draw(graphics2D);
        }

        // kirajzoljuk az aktív jobb oldali főnixeket
        for (Phoenix phoenix : phoenixesFromRight) {
            phoenix.draw(graphics2D);
        }

        // kirajzoljuk a tűzgolyókat
        for (Fireball fireball : fireballs) {
            fireball.draw(graphics2D);
        }

        // kirajzoljuk a vízcseppeket
        for (Droplet droplet : droplets) {
            droplet.draw(graphics2D);
        }

        // kirajzoljuk a játék felső sarkaiba az aktuális game session információit:
        //  pontszám, megmaradt életek, játékos neve, és szintje
        graphics2D.drawString("SCORE - " + score, 40, 50);
        graphics2D.drawString("LEVEL - " + level, 40, 80);
        graphics2D.drawString(playerName, CUSTOM_WIDTH - 200, 50);
        graphics2D.drawString("LIVES - " + lives, CUSTOM_WIDTH - 200, 80);

        // ha elfogytak a karakterünk életei, kirajzoljuk nagyba a game over szöveget
        if (lives == 0) {
            graphics2D.setFont(customFont.deriveFont(102f));
            graphics2D.drawString("GAME OVER", 240, 500);
        }

        // ha a szüneteltetés páratlan másodpercében vagyunk, kijazoljuk nagyba a szüneteltetett játék szöveget
        if (pause && writePause < 10) {
            graphics2D.setFont(customFont.deriveFont(102f));
            graphics2D.drawString("GAME PAUSED", 170, 500);
        }
    }

    // játék változóinak frissítése
    private void updateGame() {
        // ha nem megy a háttérzene, mert még nem lett elindítva, vagy mert le lett állítva egy szüneteltetéssel,
        //  újraindítjuk a zenét
        if (!bgMusicPlays) {
            new Thread(bgMusicController).start();
            bgMusicPlays = true;
        }

        // ha kell és lehet is, új főnix(ek)et rajzolunk ki
        spawnPhoenix();

        // frissítjük a főnixek pozícióit, és figyeljük, eltalálja-e valamelyiket egy-egy vízcseppünk
        updatePhoenix();

        // frissítjük a tűzgolyók helyzetét
        for (int i = 0; i < fireballs.size(); i++) {
            Fireball fireball = fireballs.get(i);
            fireball.update();
            // ha már nem láthatóak a panelen, eltávolítjuk az adott tűzgolyót a listánkból
            if (fireball.getY() > CUSTOM_HEIGHT) {
                fireballs.remove(fireball);
            }

            // ha egy tűzgolyó eltalálja a karakterünket, meghívjuk az ezt kezelő privát metódust
            if (pear.getBound().intersects(fireball.getBound()) && lives > 0) {
                onLostLife(fireball);
            }
        }

        // frissítjük a vízcseppek helyzetét
        for (int i = 0; i < droplets.size(); i++) {
            Droplet droplet = droplets.get(i);
            droplet.update();
            // ha már nem láthatóak a panelen, eltávolítjuk az adott tűzgolyót a listánkból
            if (droplet.getY() < 0) {
                droplets.remove(droplet);
            }
        }
    }

    // privát metódus az új képkocka kirajzolására
    private void renderGame() {
        repaint();
    }

    // új főnix kirajzolására metódus
    private void spawnPhoenix() {
        if (phoenixesFromLeft.size() + phoenixesFromRight.size() < maxPhoenixes) {  // ha még nem értük el az aktív főnixek max. számát
            currentY++; // magasság változó, ez alapján és a getPhoenixY metódus segítségével, felhasználva a
            // phoenixPositions konstans tömböt, meg fogjuk kapni, hogy hova generálható az új főnix

            // random döntjük el, hogy jobbról vagy balról induljon az új főnix
            int spawnSide = getRandomValue(0, 1);

            // a beállított változók alapján a megfelelő pozícióra generáljuk az új főnixet
            if (spawnSide == 0) {
                phoenixesFromLeft.add(new Phoenix(-124, getPhoenixY(), getRandomValue(1, 5), spawnSide));
            } else {
                phoenixesFromRight.add(new Phoenix(CUSTOM_WIDTH, getPhoenixY(), getRandomValue(1, 5), spawnSide));
            }
        }
    }

    // a spawnPhoenixben beállított currentY változó alapján meghatározzuk, hogy a konsans tömbünk által meghatározott
    //  hanyadik pozícióra kell generáljuk az új főnixet
    private int getPhoenixY() {
        if (currentY >= phoenixPositions.length) {
            currentY = 0;
        }
        return phoenixPositions[currentY];
    }

    // frissítjük a főnixek pozícióit, és figyeljük, eltalálja-e valamelyiket egy-egy vízcseppünk
    private void updatePhoenix() {
        // bal oldaliakra
        for (int i = 0; i < phoenixesFromLeft.size(); i++) {
            Phoenix phoenix = phoenixesFromLeft.get(i);
            phoenix.updateLeft();

            // ha a mostani főnixünk ütközik egy vízcseppel, eltávolítjuk a listából és így a képernyőről is
            collision(phoenix, phoenixesFromLeft);

            // akkor is eltávolítjuk a listából, ha kirepül a képernyőről
            if (phoenix.getX() > CUSTOM_WIDTH) {
                phoenixesFromLeft.remove(phoenix);
            }
        }

        // jobb oldaliakra
        for (int i = 0; i < phoenixesFromRight.size(); i++) {
            Phoenix phoenix = phoenixesFromRight.get(i);
            phoenix.updateRight();

            // ha a mostani főnixünk ütközik egy vízcseppel, eltávolítjuk a listából és így a képernyőről is
            collision(phoenix, phoenixesFromRight);

            // akkor is eltávolítjuk a listából, ha kirepül a képernyőről
            if (phoenix.getX() < -124) {
                phoenixesFromRight.remove(phoenix);
            }
        }
    }

    // főnix vízcseppel való ütközését kezelő metódus
    private void collision(Phoenix phoenix, ArrayList<Phoenix> phoenixes) {
        // végigmegyünk mindegyik aktív vízcseppen és megnézzük, ütközik-e valamelyik a jelenlegi főnixszel
        for (int j = 0; j < droplets.size(); j++) {
            Droplet droplet = droplets.get(j);
            if (phoenix.getBound().intersects(droplet.getBound())) {    // ha igen
                // lejátszuk a megfelelő hangeffektet
                Controller collisionFXController = new Controller("assets/audio/kill.wav", false, FX_DEC);
                new Thread(collisionFXController).start();

                // eltávolítjuk a főnixet is, a vízcseppet is
                phoenixes.remove(phoenix);
                droplets.remove(droplet);
                score += phoenix.getSpeed();    // annyival növeljük a pontszámot, amilyen sebességű főnixet találtunk el

                // pontszám és szint szerint növeljük a szintet és a maximum megjeleníthető főnixek számát (mindezt
                //  mindegyik szintlépés után egyre nagyobb léptékkel)
                if (score >= level * INIT_LEVELUP_INTERVAL + (level - 1) * 10) {
                    level++;
                    maxPhoenixes++;

                    // és lejátszuk a megfelelő hangeffektet
                    Controller levelUpFXController = new Controller("assets/audio/level up.wav", false, FX_DEC);
                    new Thread(levelUpFXController).start();
                }
            }
        }

        // a paraméterként kapott főnixtől random indítunk egy új tűzgolyót, random sebességgel
        if (getRandomValue(0, 170) == 0) {
            fireballs.add(new Fireball(phoenix.getX() + 64, phoenix.getY() + 130, getRandomValue(MIN_FIREBALL_SPEED, MAX_FIREBALL_SPEED)));
        }
    }

    // privát metódus, mely elvégzi a megfelelő műveleteket, amikor eltalálja karakterünket egy tűzgolyó
    private void onLostLife(Fireball fireball) {
        // csökken a hátralevő életek száma
        lives--;

        // lejátszuk a megfelelő hangeffektet
        Controller lostLifeFXController = new Controller("assets/audio/lives--.wav", false, FX_DEC);
        new Thread(lostLifeFXController).start();

        if (lives > 0) {
            // ha még maradt életünk, csak eltávolítjuk a tűzgolyót, amely eltalált minket
            fireballs.remove(fireball);
        } else {
            // ha elfogyott minden élet, frissítjük a leaderboardot
            Leaderboard leaderboard = new Leaderboard();
            leaderboard.insert(new Player(playerName, level, score));

            // leállítjuk a háttérzenét
            bgMusicController.stop();

            // lejátszuk a game over zenét
            Controller gameOverController = new Controller("assets/audio/game over.wav", false, FX_DEC);
            gameOverThread = new Thread(gameOverController);
            gameOverThread.start();

            // leállítjuk a run metódusban futó ciklist, aka. a játék futását
            isRunning = false;
        }
    }

    // adott intervallumon belüli random érték generálása
    private int getRandomValue(int a, int b) {
        return new Random().nextInt(b - a + 1) + a;
    }
}
