// Demian Ervin - István

// Játékablakot implementáló osztály

package game;

import util.Common;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameFrame extends JFrame implements Common {
    public GameFrame(String playerName) {
        // játékablak beállításai: cím, bezárásí művelet
        setTitle("DisapPear Game");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // ha a menübe nem írtak nevet, PLAYER lesz a név
        if (playerName.isBlank()) {
            playerName = "PLAYER";
        }
        // példányosítjuk a játékpanelt, aka. elinditjuk a játékot
        add(new GamePanel(this, playerName));

        // beállítjuk az ablak ikonját
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("assets/img/pear.png"));
        } catch (IOException ignored) {
        }
        setIconImage(img);

        // további beállításai a játékablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setResizable(false);
        pack();
        setLocationRelativeTo(null);  // igy a kepernyo kozepere fog kerulni az ablak
        setVisible(true);
    }
}
