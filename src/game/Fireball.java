// Demian Ervin - István

// A főnixek tűzgolyó lövedékének implementálása

package game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Fireball extends Sprite {
    // Betöltjük a tűzgolyó képet
    BufferedImage img;

    public Fireball(int x, int y, int speed) {
        super(x, y, speed);
        try {
            img = ImageIO.read(new File("assets/img/fireball.png"));
        } catch (IOException ignored) {
        }
    }

    // Helyzetfrissítési metódus, a tűzgolyó jelenlegi magasságának és beállított sebességének felhasználásával
    public void update() {
        setY(getY() + getSpeed());
    }

    // Tűzgolyó jelenlegi helyzetét meghatározó téglalap objektum generáló és visszatérítő, publikus metódus
    public Rectangle2D getBound() {
        return new Rectangle(getX(), getY(), this.img.getWidth(null), this.img.getHeight(null));
    }

    // Tűzgolyó kirajzolása a megfelelő pozícióra
    @Override
    protected void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(img, getX(), getY(), null);
    }
}
