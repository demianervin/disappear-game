// Demian Ervin - István

// Egy főnix kirajzolását megvalósító osztály implementálása

package game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Phoenix extends Sprite {
    // Betöltjük a főnix képét
    BufferedImage img;

    public Phoenix(int x, int y, int speed, int spawnSide) {
        super(x, y, speed);
        if (spawnSide == 0) {       // ha balról kéri a metódus paramétere a főnix kiindulását
            try {
                img = ImageIO.read(new File("assets/img/phoenixInv.png"));
            } catch (IOException ignored) {
            }
        } else {                    // ha jobbról kéri a metódus paramétere a főnix kiindulását
            try {
                img = ImageIO.read(new File("assets/img/phoenix.png"));
            } catch (IOException ignored) {
            }
        }
    }

    // balról induló főnix helyzetének frissítése, annak beállított sebességének függvényében
    public void updateLeft() {
        setX(getX() + getSpeed());
    }

    // jobbrül induló főnix helyzetének frissítése, annak beállított sebességének függvényében
    public void updateRight() {
        setX(getX() - getSpeed());
    }

    // főnix jelenlegi helyzetét meghatározó téglalap objektum generáló és visszatérítő, publikus metódus
    public Rectangle2D getBound() {
        return new Rectangle(getX(), getY(), this.img.getWidth(null), this.img.getHeight(null));
    }

    // főnix kirajzolása a megfelelő pozícióra
    @Override
    protected void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(img, getX(), getY(), null);
    }
}
