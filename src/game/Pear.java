// Demian Ervin - István

// A körte kirajzolását megvalósító osztály implementálása

package game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Pear extends Sprite {
    // Betöltjük a körte képét
    private BufferedImage img;

    public Pear(int x, int y, int speed) {
        super(x, y, speed);
        try {
            img = ImageIO.read(new File("assets/img/pear.png"));
        } catch (IOException ignored) {
        }
    }

    // balra mozditás metódusa, ami a megfelelő gomb megnyomásakor lesz meghívva
    public void moveLeft() {
        // frissítjük a pozícióját, hacsak nem értünk el a vászon bal szélére
        if (getX() < 0) {
            return;
        }

        // növeljük a körte sebességét, ha esetleg lenyomva tartaná a játékos a gombot
        incSpeed();
        setX(getX() - getSpeed());
    }

    // jobbra mozditás metódusa, ami a megfelelő gomb megnyomásakor lesz meghívva
    public void moveRight() {
        // frissítjük a pozícióját, hacsak nem értünk el a vászon jobb szélére
        if (getX() > CUSTOM_WIDTH - 100) {
            return;
        }

        // növeljük a körte sebességét, ha esetleg lenyomva tartaná a játékos a gombot
        incSpeed();
        setX(getX() + getSpeed());
    }

    // sebesség növelését megvalósító privát metódus
    private void incSpeed() {
        // csak akkor növelünk sebességet, ha még nem értük el a globálisan beállított maximum éréket (Common interface)
        if (getSpeed() < MAX_SPEED) {
            setSpeed(getSpeed() + 1);
        }
    }

    // globálisan beállított (Common interface) sebesség visszaállítását megvalósító metódus, mely egy adott irányítási
    //  gomb elengedésekor lesz meghívva
    public void speedReset() {
        setSpeed(INIT_SPEED);
    }

    // Körte jelenlegi helyzetét meghatározó téglalap objektum generáló és visszatérítő, publikus metódus
    public Rectangle2D getBound() {
        return new Rectangle(getX() + 20, getY() + 20, 60, 150);
    }

    // Körte kirajzolása a megfelelő pozícióra
    @Override
    protected void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(img, getX(), getY(), null);
    }
}
