// Demian Ervin - István

// A játék egy objektumának megjelenítését előkészítő absztrakt osztály

package game;

import util.Common;

import java.awt.*;

public abstract class Sprite implements Common {
    // konstruktora tartalmazza az elem kezdő pozícióját és mozgási sebességét
    private int x, y, speed;

    public Sprite(int x, int y, int speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    // megfelelő getter és setter metódusok
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    // Az elem kirajzolását előkészítő absztrakt metódus
    protected abstract void draw(Graphics2D graphics2D);

}
