// Demian Ervin - István

// A körte vízcspp lövedékének implementálása

package game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Droplet extends Sprite {
    // Betöltjük a vízcsepp képet
    BufferedImage img;

    public Droplet(int x, int y, int speed) {
        super(x, y, speed);
        try {
            img = ImageIO.read(new File("assets/img/droplet.png"));
        } catch (IOException ignored) {
        }
    }

    // Helyzetfrissítési metódus, a vízcsepp jelenlegi magasságának és beállított sebességének felhasználásával
    public void update() {
        setY(getY() - getSpeed());
    }

    // Vízcsepp jelenlegi helyzetét meghatározó téglalap objektum generáló és visszatérítő, publikus metódus
    public Rectangle2D getBound() {
        return new Rectangle(getX(), getY(), this.img.getWidth(null), this.img.getHeight(null));
    }

    // Vízcsepp kirajzolása a megfelelő pozícióra
    @Override
    protected void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(img, getX(), getY(), null);
    }
}
