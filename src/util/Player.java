// Demian Ervin - István

// játékos adatait összesítő osztály

package util;

public class Player {
    // példányában lementi egy játékos nevét, elért szintjét és pontszámát
    private final String name;
    private final int level;
    private final int score;

    public Player(String name, int level, int score) {
        this.name = name;
        this.level = level;
        this.score = score;
    }

    // getter függvények
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public int getScore() {
        return score;
    }

    // stringgé alakítás, leaderboardba való kiíráshoz vagy onnan exporthoz/mentéshez
    @Override
    public String toString() {
        return name + "\t-\t" +
                level + "\t-\t" +
                score;
    }
}
