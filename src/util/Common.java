// Demian Ervin - István

// Common interface, globális változók tárolására, gyors elérésére és módosítására

package util;

public interface Common {
    int CUSTOM_WIDTH = 1300;
    int CUSTOM_HEIGHT = 980;

    long MILLISECONDS = 1000L;

    int FPS = 30;

    int INIT_SPEED = 10;
    int MAX_SPEED = 35;

    int MIN_DROPLET_SPEED = 5;
    int MAX_DROPLET_SPEED = 15;
    int MAX_DROPLETS = 15;

    int MIN_FIREBALL_SPEED = 2;
    int MAX_FIREBALL_SPEED = 6;

    int INIT_MAX_PHOENIXES = 5;

    int INIT_LEVELUP_INTERVAL = 30;

    int LIVES = 3;

    float MUSIC_DEC = 0.0f;
    float FX_DEC = 0.0f;
}
