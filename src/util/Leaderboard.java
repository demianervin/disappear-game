// Demian Ervin - István

// Leaderboardot implementáló osztály

package util;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Leaderboard {
    private List<Player> players = new ArrayList<>();

    // leaderboard konstruktora, kiolvassa a fájlból az adatokat vagy létrehoz egy fájlt, ha nincs
    public Leaderboard() {
        boolean fileExists = false;

        try {
            File file = new File("assets/ldb");
            fileExists = file.createNewFile();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        if (!fileExists) {
            try {
                BufferedReader o = new BufferedReader(new FileReader("assets/ldb"));
                String plyrs;
                try {
                    while ((plyrs = o.readLine()) != null) {

                        String[] arr = plyrs.split("\t-\t");

                        players.add(new Player(arr[0], Integer.parseInt(arr[1]), Integer.parseInt(arr[2])));
                    }
                    sort();
                } catch (IOException ignored) {
                }
            } catch (FileNotFoundException ignored) {
            }
        }
    }

    // új adat beszúrását megvalósító metódus
    public void insert(Player player) {
        // hozzáadjuk a listához
        players.add(player);
        // rendezzük score szerint
        sort();
        // lementjük a fájlba a módosított leaderboardot
        save();
    }

    // getter függvény, a leaderboard panel így fogja tudni kiírni az pontok jelenlegi állását
    public Stream<Player> getPlayerStream() {
        return players.stream();
    }

    // leaderboard exportálása
    public void export(Component parent) {
        final JFileChooser fc = new JFileChooser();
        int returnVal = fc.showSaveDialog(parent); // parent component to JFileChooser
        if (returnVal == JFileChooser.APPROVE_OPTION) { // OK button pressed by user
            File file = fc.getSelectedFile(); // get File selected by user
            try {
                BufferedWriter o = new BufferedWriter(new FileWriter(file)); // use its name
                players.stream().map(Player::toString).forEach(s -> {
                    try {
                        o.write(s + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                o.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // leaderboard importálása
    public void importL(Component parent) {
        final JFileChooser fc = new JFileChooser();
        int returnVal = fc.showSaveDialog(parent); // parent component to JFileChooser
        if (returnVal == JFileChooser.APPROVE_OPTION) { // OK button pressed by user
            File file = fc.getSelectedFile(); // get File selected by user
            // ha kiválasztott egy fájlt, ürítjük a mostani játékoslistát
            players = new ArrayList<>();
            // megpróbáljuk beolvasni az új játékoslistát
            try {
                BufferedReader o = new BufferedReader(new FileReader(file));
                String plyrs;
                try {
                    // végigmegyünk a sorokon, kiválogatjuk belőlük a játékosok neveit, elért szintjeiket és pontszámukat
                    while ((plyrs = o.readLine()) != null) {
                        String[] arr = plyrs.split("\t-\t");
                        players.add(new Player(arr[0], Integer.parseInt(arr[1]), Integer.parseInt(arr[2])));
                    }
                    // rendezzük score szerint
                    sort();
                    // ha minden jól ment, lementjük az új leaderboardot
                    save();
                } catch (IOException ignored) {
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // leaderboard ürítése, azaz ráírunk a mostani fájlra egy üres file-t
    public void clean() {
        try {
            new PrintWriter("assets/ldb").close();
        } catch (FileNotFoundException ignored) {
        }
    }

    // leaderboard mentése
    private void save() {
        try {
            BufferedWriter o = new BufferedWriter(new FileWriter("assets/ldb")); // use its name
            players.stream().map(Player::toString).forEach(s -> {
                try {
                    o.write(s + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            o.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // leaderboard rendezése, mindig az a 10 legtöbb pontszámú játékost hagyjuk meg
    private void sort() {
        players = players.stream()
                .sorted(Comparator.comparing(Player::getScore).reversed())
                .limit(10)
                .collect(Collectors.toList());
    }
}
