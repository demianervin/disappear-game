// Demian Ervin - István

// absztrakt osztály, előkészíti a GamePanel-t

package util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public abstract class Canvas extends JPanel implements KeyListener, Common {

    public Canvas() {
        this.addKeyListener(this);
        setDoubleBuffered(true);        // képi minőségbeli javítás
        setFocusable(true);
        requestFocus();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        onDraw(graphics2D);
    }

    // keyListenerhez szűkséges metódusok inicializása (a használt saját metódusok absztraktok és a GamePanel-ben vannak implementálva)
    @Override
    public void keyPressed(KeyEvent e) {
        onKeyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        onKeyReleased(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    protected abstract void onKeyPressed(KeyEvent keyEvent);

    protected abstract void onKeyReleased(KeyEvent keyEvent);

    protected abstract void onDraw(Graphics2D graphics2D);
}
