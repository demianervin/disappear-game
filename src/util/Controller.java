// Demian Ervin - István

// Zenét lejátszó threadhez controller osztály

package util;

public class Controller implements Runnable {
    private final String musicPath;
    private final boolean loop;
    private final float decVol;
    private Sound sound;

    // konstruktor, beállítja a lejátszandó hangfájl elérési útvonalát, azt, hogy loop-olandó-e, és hogy mennyivel
    //  legyen csökkentve a hangereje
    public Controller(String musicPath, boolean loop, float decVol) {
        this.musicPath = musicPath;
        this.loop = loop;
        this.decVol = decVol;
    }

    // Runnable osztály, a futás annyiból áll, hogy beállítjuk a hangerőt, és megfelelően elindítjuk a Sound objektumot
    @Override
    public void run() {
        sound = new Sound(musicPath);

        decVol();

        if (loop) {
            sound.loop();
        } else {
            sound.play();
        }
    }

    // metódus, mely egy zene leállítását valósítja meg
    public void stop() {
        sound.stop();
    }

    // metódus, mely egy zene hangerejének adott mértékű csökkentését valósítja meg
    public void decVol() {
        sound.decreaseVolume(decVol);
    }
}
