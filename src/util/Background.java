// Demian Ervin - István

// háttért betöltő osztály

package util;

import game.Sprite;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Background extends Sprite {
    // betöltjük a hátteret
    private BufferedImage img;

    public Background(int x, int y) {
        super(x, y, 0);
        try {
            img = ImageIO.read(new File("assets/img/bg.jpg"));
        } catch (IOException ignored) {
        }
    }

    // kirajzoljuk
    @Override
    public void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(img, getX(), getY(), null);
    }
}
