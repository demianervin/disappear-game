// Demian Ervin - István

// egy hangfájl betöltését és kezelését megvalósító osztály

package util;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class Sound implements Common {
    // betöltjük a hangfájlt egy Clip típusú változóba
    private Clip clip;

    public Sound(String path) {
        File music = new File(path);

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(music);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    // metódus, hangfájl elindításához, ismétlés nélkül
    public void play() {
        try {
            clip.start();
            do {
                Thread.sleep(15);
            } while (clip.isRunning());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // ha lejátszódott a hangfájl, bezárjuk
            try {
                clip.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // metódus, hangfájl elindításához, ismétléssel
    public void loop() {
        try {
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // metódus, hangfájl leállításához, ismétléssel
    public void stop() {
        clip.stop();
        clip.setMicrosecondPosition(0);
    }

    // metódus, hangfálj eredeti hangerejének csökkentéséhez
    public void decreaseVolume(float decibel) {
        FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        volume.setValue(decibel);
    }
}
