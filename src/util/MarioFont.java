// Demian Ervin - István

// Saját betűtípus betöltését megvalósító osztály

package util;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class MarioFont {
    private Font customMarioFont;

    // konstruktor, betöltjük
    public MarioFont() {
        try {
            customMarioFont = Font.createFont(Font.TRUETYPE_FONT, new File("assets/textfont/SuperMarioWorldTextBoxRegular-Y86j.ttf")).deriveFont(12f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

            ge.registerFont(customMarioFont);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }
    }

    // méretet beállító metódus
    public Font getCustomMarioFont(float size) {
        return customMarioFont.deriveFont(size);
    }
}
