// Demian Ervin - István

// Játékmenüt megvalósító osztály

package util;

import dialog.AboutDialog;
import dialog.ControlsDialog;
import dialog.ExitDialog;
import dialog.LeaderboardDialog;
import game.GameFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MenuFrame extends JFrame {

    public MenuFrame() {
        // menü ablak beállításai: cím, bezárásí művelet
        setTitle("DisapPear Game Launcher");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        getContentPane().setBackground(Color.black);

        // Mivel a JFrame-re több panelt fogunk helyezni, szűkséges a layout null-ra állítása, mert az alapbeállítás a FlowLayout
        setLayout(null);

        // saját betűtípus példányosítása és felhasználása
        Font customMarioFont = new MarioFont().getCustomMarioFont(56f);
        setFont(customMarioFont);

        // készítünk egy panelt, amin csak a cím cimkéje lesz
        JPanel titlePanel = new JPanel();
        titlePanel.setBounds(35, 40, 600, 60);
        titlePanel.setBackground(Color.black);

        // cím cimkéje
        JLabel titleLabel = new JLabel("DISAPPEAR");
        titleLabel.setForeground(Color.white);
        titleLabel.setFont(customMarioFont);
        titlePanel.add(titleLabel);
        add(titlePanel);

        // csökkentjük a betűtípus méretét
        customMarioFont = customMarioFont.deriveFont(21f);

        // készítünk egy panelt, amin a játékos nevét fogjuk kérni beírásra
        JPanel namePanel = new JPanel();
        namePanel.setBounds(80, 150, 500, 40);
        namePanel.setBackground(Color.black);

        // címke, a játékos nevét kéri
        JLabel nameLabel = new JLabel("ENTER YOUR NAME  ");
        nameLabel.setForeground(Color.white);
        nameLabel.setFont(customMarioFont);

        // szövegdoboz, a játékos nevét várja beírásra
        JTextField nameTextField = new JTextField(10);
        nameTextField.setBackground(Color.black);
        nameTextField.setForeground(Color.white);
        nameTextField.setFont(customMarioFont);
        nameTextField.setHorizontalAlignment(SwingConstants.CENTER);
        nameTextField.addKeyListener(new KeyAdapter() {     // enter nyomására elindul a játék
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new GameFrame(nameTextField.getText());
                    dispose();
                }
            }
        });

        namePanel.add(nameLabel);
        namePanel.add(nameTextField);
        add(namePanel);

        // készítünk egy panelt a gomboknak
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(6, 1));
        buttonPanel.setBounds(210, 210, 250, 230);
        buttonPanel.setBackground(Color.black);

        // elkészítjük az új játék gombot
        JButton newGameButton = new JButton("NEW GAME");
        newGameButton.setBackground(Color.black);
        newGameButton.setForeground(Color.white);
        newGameButton.setFont(customMarioFont);
        newGameButton.addMouseListener(new MouseAdapter() {     // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                new GameFrame(nameTextField.getText());
                dispose();
            }
        });
        newGameButton.addKeyListener(new KeyAdapter() {         // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new GameFrame(nameTextField.getText());
                    dispose();
                }
            }
        });

        // elkészítjük a kilépés gombot
        JButton exitButton = new JButton("EXIT");
        exitButton.setBackground(Color.black);
        exitButton.setForeground(Color.white);
        exitButton.setFont(customMarioFont);
        MenuFrame menuFrame = this;
        exitButton.addMouseListener(new MouseAdapter() {        // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                new ExitDialog(menuFrame);
            }
        });
        exitButton.addKeyListener(new KeyAdapter() {            // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new ExitDialog(menuFrame);
                }
            }
        });

        // elkészítjük az irányítás infó gombot
        JButton controlsButton = new JButton("CONTROLS");
        controlsButton.setBackground(Color.black);
        controlsButton.setForeground(Color.white);
        controlsButton.setFont(customMarioFont);
        controlsButton.addMouseListener(new MouseAdapter() {        // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                new ControlsDialog();
            }
        });
        controlsButton.addKeyListener(new KeyAdapter() {            // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new ControlsDialog();
                }
            }
        });

        // elkészítjük a játékot bemutató about gombot
        JButton aboutButton = new JButton("ABOUT");
        aboutButton.setBackground(Color.black);
        aboutButton.setForeground(Color.white);
        aboutButton.setFont(customMarioFont);
        aboutButton.addMouseListener(new MouseAdapter() {       // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                new AboutDialog();
            }
        });
        aboutButton.addKeyListener(new KeyAdapter() {           // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new AboutDialog();
                }
            }
        });

        // elkészítjük a leaderboard gombot
        JButton leaderboardButton = new JButton("LEADERBOARD");
        leaderboardButton.setBackground(Color.black);
        leaderboardButton.setForeground(Color.white);
        leaderboardButton.setFont(customMarioFont);
        leaderboardButton.addMouseListener(new MouseAdapter() {     // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                new LeaderboardDialog();
            }
        });
        leaderboardButton.addKeyListener(new KeyAdapter() {         // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new LeaderboardDialog();
                }
            }
        });

        // hozzáadjuk a gombokat a panelhez
        buttonPanel.add(newGameButton);
        buttonPanel.add(controlsButton);
        buttonPanel.add(leaderboardButton);
        buttonPanel.add(aboutButton);
        buttonPanel.add(exitButton);
        add(buttonPanel);

        // ikont állítunk be az ablaknak
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("assets/img/pear.png"));
        } catch (IOException ignored) {
        }
        setIconImage(img);

        // további beállításai a menü ablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setSize(700, 480);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
