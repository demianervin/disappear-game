// Demian Ervin - István

// Menüből elérhető információs párbeszédablak implementálása, mit kell tudni az irányításról, mik a játék céljai

package dialog;

import util.MarioFont;

import javax.swing.*;
import java.awt.*;

public class ControlsDialog extends JDialog {
    public ControlsDialog() {
        Font customFont = new MarioFont().getCustomMarioFont(16f);  // saját betűtípus példányosítása, később felhasználása

        // párbeszédablak beállításai: cím, egyszer megnyitható + bezárásí művelet
        setTitle("Controls");
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        // készítünk egy panelt
        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(null);
        controlsPanel.setBackground(Color.black);
        controlsPanel.setForeground(Color.white);
        controlsPanel.setBounds(0, 0, 650, 600);

        // a panelre ráhelyezzük a megfelelő szöveget egy nem szerkeszthető JTextArea által
        JTextArea textArea = new JTextArea();
        textArea.setBackground(Color.black);
        textArea.setForeground(Color.white);
        textArea.setEditable(false);
        textArea.setBounds(20, 20, 380, 320);
        textArea.setFont(customFont);
        textArea.setText("""
                 ----((( CONTROLS )))----
                 
                LEFT ARROW - Move to left
                                
                RIGHT ARROW - Move to right
                                
                SPACE BAR - Shoot
                                
                ESC - Pause
                                
                                
                 ---((( OBJECTIVES )))---
                 
                STAY ALIVE AS THE GAME
                                
                    GETS HARDER AND HARDER
                                
                MAKE PHOENIXES DISAPPEAR
                                
                BEAT THE HIGH SCORE""");

        controlsPanel.add(textArea);
        add(controlsPanel);

        // további beállításai a párbeszédablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setSize(420, 400);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
