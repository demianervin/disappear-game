// Nev: Demian Ervin-Istvan

// Menüből elérhető interaktív párbeszédablak implementálása, leaderboard megtekintéséhez és kezeléséhez

package dialog;

import util.Player;
import util.Leaderboard;
import util.MarioFont;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.stream.Stream;

public class LeaderboardDialog extends JDialog {
    JTextArea textArea;

    public LeaderboardDialog() {
        // párbeszédablak beállításai: cím, egyszer megnyitható, bezárásí művelet
        setTitle("Leaderboard");
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        /* Mivel a JDialog-ra több panelt fogunk helyezni, szűkséges a háttérszín beállítása + layout null-ra állítása,
            mert az alapbeállítás a FlowLayout */
        getContentPane().setBackground(Color.black);
        setLayout(null);

        Font customMarioFont = new MarioFont().getCustomMarioFont(18f); // saját betűtípus példányosítása, később felhasználása

        // készítünk egy panelt, ami csakis a  leaderboard mejelenítését fogja szolgálni
        JPanel leaderboardPanel = new JPanel();
        leaderboardPanel.setLayout(null);
        leaderboardPanel.setBackground(Color.black);
        leaderboardPanel.setForeground(Color.white);
        leaderboardPanel.setBounds(0, 0, 700, 460);

        // a panelre ráhelyezzük a megfelelő szöveget egy JTextArea által
        textArea = new JTextArea();
        textArea.setBackground(Color.black);
        textArea.setForeground(Color.white);
        textArea.setEditable(false);
        textArea.setBounds(20, 20, 700, 460);
        textArea.setFont(customMarioFont);
        update();   // lekérjük az aktuális leaderboard adatokat

        // készítünk egy panelt, ami csakis a leaderboard kezeléséhez szűkséges gombokat fogja tartalmazni
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(3, 1));
        buttonPanel.setBounds(18, 460, 640, 150);
        buttonPanel.setBackground(Color.black);

        // elkészítjük az export gombot
        JButton exportLeaderboardButton = new JButton("EXPORT LEADERBOARD");
        exportLeaderboardButton.setBackground(Color.black);
        exportLeaderboardButton.setForeground(Color.white);
        exportLeaderboardButton.setFont(customMarioFont);
        exportLeaderboardButton.addMouseListener(new MouseAdapter() {           // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {        // a gomb aktiválható klikkeléssel
                super.mouseEntered(e);
                new Leaderboard().export(exportLeaderboardButton);

            }
        });
        exportLeaderboardButton.addKeyListener(new KeyAdapter() {               // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {               // vagy TAB-os kijelölés utáni enter gomb lenyomással
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new Leaderboard().export(exportLeaderboardButton);
                }
            }
        });

        // elkészítjük a betöltés gombot
        JButton loadButton = new JButton("IMPORT LEADERBOARD");
        loadButton.setBackground(Color.black);
        loadButton.setForeground(Color.white);
        loadButton.setFont(customMarioFont);
        loadButton.addMouseListener(new MouseAdapter() {            // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                new Leaderboard().importL(loadButton);
                update();
            }
        });
        loadButton.addKeyListener(new KeyAdapter() {                // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {        // vagy TAB-os kijelölés utáni enter gomb lenyomással
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new Leaderboard().importL(loadButton);
                    update();
                }
            }
        });

        // elkészítjük a tisztítás/kiürítés gombot
        JButton clearButton = new JButton("CLEAR LEADERBOARD");
        clearButton.setBackground(Color.black);
        clearButton.setForeground(Color.white);
        clearButton.setFont(customMarioFont);
        clearButton.addMouseListener(new MouseAdapter() {           // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {        // a gomb aktiválható klikkeléssel
                super.mouseEntered(e);
                new Leaderboard().clean();
                update();
            }
        });
        clearButton.addKeyListener(new KeyAdapter() {               // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {        // vagy TAB-os kijelölés utáni enter gomb lenyomással
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new Leaderboard().clean();
                    update();
                }
            }
        });

        // a panelekre felhejezzük a megfelelő objektumokat, majd a paneleket a párbeszédablakhoz adjuk
        leaderboardPanel.add(textArea);
        buttonPanel.add(exportLeaderboardButton);
        buttonPanel.add(loadButton);
        buttonPanel.add(clearButton);
        add(leaderboardPanel);
        add(buttonPanel);

        // további beállításai a párbeszédablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setSize(700, 660);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    // privát metódus, ami szűkség esetén lekéri a leaderboard naprakészen tartásához szűkséges adatokat
    private void update() {
        textArea.setText("NAME\t-      LEVEL\t-\tSCORE\n\n\n");                 // leaderboard fejléce
        Stream<Player> playerStream = new Leaderboard().getPlayerStream();      // adatok lekérése
        playerStream.map(Player::toString).forEach(s -> textArea.append(s + "\n\n"));   // adatok kiírása a JTextAreára
    }
}
