// Demian Ervin - István

// Menüből elérhető interaktív párbeszédablak implementálása, kilépéskor megérdi a felhasználót, biztos-e a kilépésben

package dialog;

import util.MarioFont;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ExitDialog extends JDialog {
    public ExitDialog(JFrame menuFrame) {
        Font customFont = new MarioFont().getCustomMarioFont(21f);      // saját betűtípus példányosítása, később felhasználása

        // párbeszédablak beállításai: cím, egyszer megnyitható, bezárásí művelet
        setTitle("Exit");
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        // készítünk egy panelt
        JPanel exitPanel = new JPanel();
        exitPanel.setLayout(null);
        exitPanel.setBackground(Color.black);
        exitPanel.setForeground(Color.white);
        exitPanel.setBounds(0, 0, 600, 200);

        // a panelre ráhelyezzük a megfelelő szöveget egy JLabel által
        JLabel exitLabel = new JLabel("Are you sure you want to exit?");
        exitLabel.setBounds(30, 15, 600, 50);
        exitLabel.setForeground(Color.white);
        exitLabel.setBackground(Color.black);
        exitLabel.setFont(customFont);

        // elkészítjük az igen gombot
        JButton yesButton = new JButton("YES");
        yesButton.setBounds(145, 80, 100, 50);
        yesButton.setBackground(Color.black);
        yesButton.setForeground(Color.white);
        yesButton.setFont(customFont);
        yesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {      // a gomb aktiválható klikkeléssel
                super.mouseClicked(e);
                menuFrame.dispose();
                dispose();
            }
        });
        yesButton.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {            // vagy TAB-os kijelölés utáni enter gomb lenyomással
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    menuFrame.dispose();
                    dispose();
                }
            }
        });

        // elkészítjük az nem gombot
        JButton noButton = new JButton("NO");
        noButton.setBounds(325, 80, 100, 50);
        noButton.setBackground(Color.black);
        noButton.setForeground(Color.white);
        noButton.setFont(customFont);
        noButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {        // a gomb aktiválható klikkeléssel
                super.mouseClicked(e);
                dispose();
            }
        });
        noButton.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {              // vagy TAB-os kijelölés utáni enter gomb lenyomással
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    dispose();
                }
            }
        });

        exitPanel.add(exitLabel);
        exitPanel.add(yesButton);
        exitPanel.add(noButton);
        add(exitPanel);

        // további beállításai a párbeszédablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setSize(600, 200);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
