// Demian Ervin - István

/*
    Interaktív párbeszédablak implementálása, az összes élet elvesztése után automatikusan jelenik meg,
    megkérdi a játékost, hogy ki szeretne-e lépni a játékból, vagy inkább visszalépne a főmenübe, például egy esetleges
    új játékért
*/

package dialog;

import util.MarioFont;
import util.MenuFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GameOverDialog extends JDialog {
    public GameOverDialog() {
        Font customFont = new MarioFont().getCustomMarioFont(18f);      // saját betűtípus példányosítása, később felhasználása

        // párbeszédablak beállításai: cím, egyszer megnyitható, bezárásí művelet
        setTitle("GAME OVER");
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        // készítünk egy panelt
        JPanel gameOverPanel = new JPanel();
        gameOverPanel.setLayout(null);
        gameOverPanel.setBackground(Color.black);
        gameOverPanel.setForeground(Color.white);
        gameOverPanel.setBounds(0, 0, 600, 50);

        // a panelre ráhelyezzük a megfelelő szöveget egy JLabel által
        JLabel lostLabel = new JLabel("YOU'VE LOST ALL OF YOUR LIVES");
        lostLabel.setBounds(70, 20, 600, 50);
        lostLabel.setForeground(Color.white);
        lostLabel.setBackground(Color.black);
        lostLabel.setFont(customFont);

        // elkészítjük a főmenübe visszatéréses gombot
        JButton returnToMenuButton = new JButton("RETURN TO MENU");
        returnToMenuButton.setBounds(40, 80, 280, 50);
        returnToMenuButton.setBackground(Color.black);
        returnToMenuButton.setForeground(Color.white);
        returnToMenuButton.setFont(customFont);
        returnToMenuButton.addMouseListener(new MouseAdapter() {            // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                new MenuFrame();
                dispose();
            }
        });
        returnToMenuButton.addKeyListener(new KeyAdapter() {                // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    new MenuFrame();
                    dispose();
                }
            }
        });

        // elkészítjük a kilépés gombot
        JButton exitButton = new JButton("EXIT");
        exitButton.setBounds(380, 80, 150, 50);
        exitButton.setBackground(Color.black);
        exitButton.setForeground(Color.white);
        exitButton.setFont(customFont);
        exitButton.addMouseListener(new MouseAdapter() {            // a gomb aktiválható klikkeléssel
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseEntered(e);
                dispose();
            }
        });
        exitButton.addKeyListener(new KeyAdapter() {                // vagy TAB-os kijelölés utáni enter gomb lenyomással
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    dispose();
                }
            }
        });

        gameOverPanel.add(lostLabel);
        gameOverPanel.add(returnToMenuButton);
        gameOverPanel.add(exitButton);
        add(gameOverPanel);

        // további beállításai a párbeszédablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setSize(610, 200);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
