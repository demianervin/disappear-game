// Demian Ervin - István

// Menüből elérhető információs párbeszédablak implementálása, a játék gyors bemutatásával

package dialog;

import util.MarioFont;

import javax.swing.*;
import java.awt.*;

public class AboutDialog extends JDialog {
    public AboutDialog() {
        Font customFont = new MarioFont().getCustomMarioFont(16f);      // saját betűtípus példányosítása, később felhasználása

        // párbeszédablak beállításai: cím, egyszer megnyitható + bezárásí művelet
        setTitle("About");
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        // készítünk egy panelt
        JPanel aboutPanel = new JPanel();
        aboutPanel.setLayout(null);
        aboutPanel.setBackground(Color.black);
        aboutPanel.setForeground(Color.white);
        aboutPanel.setBounds(0, 0, 650, 600);

        // a panelre ráhelyezzük a megfelelő szöveget egy nem szerkeszthető JTextArea által
        JTextArea textArea = new JTextArea();
        textArea.setBackground(Color.black);
        textArea.setForeground(Color.white);
        textArea.setEditable(false);
        textArea.setBounds(20, 20, 610, 410);
        textArea.setFont(customFont);
        textArea.setText("""
                   'This is an easily written, open source
                 
                game with mesmerizing graphics, breathtaking
                 
                sounds and very easy controls, made only for
                                
                you to take your time and relax in the middle
                                
                of your exhausting exam session.
                                
                                
                   Also, it is a true masterpiece that, in
                                
                the creator's mother tongue is simply called
                                
                'Olcso jatek hulyegyerekeknek', which means
                                
                'exceptionally entertaining artwork'.
                                
                                
                                
                                
                ENJOY!'             -              Demian Ervin""");

        aboutPanel.add(textArea);
        add(aboutPanel);

        // további beállításai a párbeszédablaknak: nem méretezhető, képernyő középpontjába elhelyezendő, látható
        setSize(650, 450);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
